package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" CalculatorServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the Calculator Servlet");
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
		
		int total = 0;
		
		if (operation.equals("add")) {
			total = num1 + num2;
		}
		else if (operation.equals("subtract")) {
			total = num1 - num2;
		}
		else if(operation.equals("multiply")) {
			total = num1 * num2;
		}
		else if(operation.equals("divide")) {
			total = num1 / num2;
		}
		else {
			total = 0;
		}
		
		
		PrintWriter out = res.getWriter();
		
		out.println("The two numbers you provided are: " + num1 + ", " + num2);
		out.println("The operation that you wanted is: " + operation);
		out.println("The result is: " + total);
		
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the Calculator App</h1>");
		out.println("To use the app, input two numbers and an operation.<br>");
		out.println("<br>Hit the submit button after filling in the details.<br>");
		out.println("<br>You will get the result shown in your browser!");
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" CalculatorServlet has been destroy. ");
		System.out.println("******************************************");
	}
}
